<?php

declare(strict_types = 1);

function logline($format, ...$args): void
{
    printf(
        '[%s] %s' . PHP_EOL,
        date('Y-m-d H:i:s'),
        sprintf($format, ...$args)
    );
}

class Scraper
{
    protected $config;
    protected $db;
    protected $ch;

    public function __construct(array $config)
    {
        $this->config = array_merge([
            'api_url' => 'https://ws.audioscrobbler.com/2.0/',
            'img_dir' => 'images',
            'limit' => 200,
            'max_retry' => 3,
            'save_images' => true,
        ], $config);

        $this->install();

        if ($this->config['save_images']
            && !file_exists($img_dir = $this->config['img_dir'])
        ) {
            mkdir($img_dir);
        }
    }

    public function db(): PDO
    {
        return $this->db ?? $this->db = new PDO(
            'sqlite:' . $this->config['database'],
            null,
            null,
            [
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_EMULATE_PREPARES => false,
            ]
        );
    }

    public function ch()//: curl
    {
        if ($this->ch === null) {
            $this->ch = curl_init();

            curl_setopt_array($this->ch, [
                CURLOPT_TIMEOUT => 15,
                CURLOPT_SSL_VERIFYHOST => 2,
                CURLOPT_RETURNTRANSFER => true,
            ]);
        }

        return $this->ch;
    }

    public function request(string $url, array $curl_opts = null): string
    {
        curl_setopt($this->ch(), CURLOPT_URL, $url);

        if ($curl_opts) {
            curl_setopt_array($this->ch(), $curl_opts);
        }

        $result = curl_exec($this->ch());

        if ($errno = curl_errno($this->ch())) {
            throw new Exception(
                sprintf(
                    'Curl error %d - %s',
                    $errno,
                    curl_error($this->ch())
                )
            );
        }

        return $result;
    }

    public function jsonRequest(string $url, array $curl_opts = null): array
    {
        $result = $this->request($url, $curl_opts);
        $result = json_decode($result, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception(
                sprintf(
                    'Json error %d - %s',
                    json_last_error(),
                    json_last_error_msg()
                )
            );
        }

        return $result;
    }

    public function apiRequest(array $extra_params): array
    {
        $params = [
            'format' => 'json',
        ];

        if (isset($this->config['api_key'])) {
            $params['api_key'] = $this->config['api_key'];
        }

        $params = array_merge($params, $extra_params);

        $url = $this->config['api_url'] . '?' . http_build_query($params);

        $result = $this->jsonRequest($url);

        if (isset($result['error'])) {
            $err = is_array($result['error'])
                ? [
                    $result['error']['code'],
                    $result['error']['#text'],
                ]
                : [
                    $result['error'],
                    $result['message'],
                ];

            throw new Exception(
                sprintf(
                    'API error %d - %s',
                    ...$err
                )
            );
        }

        return $result;
    }

    public function install()
    {
        $this->db()->exec('
            CREATE TABLE IF NOT EXISTS `log` (
                `id`	INTEGER PRIMARY KEY AUTOINCREMENT,
                `logged`	TEXT UNIQUE,
                `track_name`	TEXT,
                `album_name`	TEXT,
                `artist_name`	TEXT,
                `track_mbid`	TEXT,
                `album_mbid`	TEXT,
                `artist_mbid`	TEXT,
                `album_image`	TEXT,
                `artist_image`	TEXT,
                `loved`	INTEGER,
                `added`	TEXT,
                `updated`	TEXT
            );
        ');
    }

    public function getRecentTracks(
        int $page = 1,
        int $before = null,
        int $after = null
    ): array {
        $params = [
            'method' => 'user.getRecentTracks',
            'limit' => $this->config['limit'],
            'user' => $this->config['user'],
            'extended' => 1,
            'page' => $page,
        ];

        if ($before !== null) {
            $params['to'] = $before;
        }

        if ($after !== null) {
            $params['from'] = $after;
        }

        logline('Fetching page %d', $page);

        $last = $this->apiRequest($params);

        return $last['recenttracks'];
    }

    public function saveTrack(array $track): bool
    {
        if (isset($track['@attr'])
            && $track['@attr']['nowplaying'] === 'true'
        ) {
            return false;
        }

        if ($this->config['save_images']) {
            !empty($track['image'])
                && $album_img = $this->saveImage($track['image']);

            !empty($track['artist']['image'])
                && $artist_img = $this->saveImage($track['artist']['image']);
        }

        $stmt = $this->db()->prepare('
            INSERT OR IGNORE INTO log (
                logged,
                track_name, album_name, artist_name,
                track_mbid, album_mbid, artist_mbid,
                album_image, artist_image,
                loved,
                added
            ) VALUES (
                ?,
                ?, ?, ?,
                ?, ?, ?,
                ?, ?,
                ?,
                CURRENT_TIMESTAMP
            )
        ');

        $data = [
            gmdate('Y-m-d H:i:s', (int) $track['date']['uts']),
            $track_name = $track['name'],
            $album_name = $track['album']['#text'],
            $artist_name = $track['artist']['#text']
                ?? $track['artist']['name']
                ?? null,
            $track['mbid'],
            $track['album']['mbid'],
            $track['artist']['mbid'],
            $album_img ?? null,
            $artist_img ?? null,
            $track['loved'] ?? 0,
        ];

        // replace empty with null
        array_map(function($a) {
            if ($a === null) {
                return null;
            }

            return strlen((string) $a) ? $a : null;
        }, $data);

        $stmt->execute($data);

        logline(
            'Logged track "%s" by "%s"',
            $track_name,
            $artist_name
        );

        return true;
    }

    public function saveTracks(array $tracks): bool
    {
        $tries = 0;

        while ($tracks) {
            $track = end($tracks);

            try {
                $this->saveTrack($track);
            } catch (Exception $e) {
                $tries++;

                logline($e->getMessage());
                logline('Retrying...');

                if ($tries <= $this->config['max_retry']) {
                    continue;
                }

                logline(
                    'Tried %d times, giving up.',
                    $this->config['max_retry']
                );
            }

            unset($tracks[key($tracks)]);
        }

        return true;
    }

    public function saveImage(array $images): ?string
    {
        $path = realpath($this->config['img_dir']);

        if (!$path) {
            throw new Exception('Image dir does not exist');
        }

        // just pick the first one, it likely exists if there are any pics
        $image = reset($images);

        if (!isset($image['#text'])) {
            return null;
        }

        if (!$image['#text']) {
            return null;
        }

        // modify the size from url to be 'original'
        $url = preg_replace(
            '#[\w]+/([^/]+)$#',
            'original/$1',
            $image['#text']
        );

        $filepath = sprintf(
            '%s/%s',
            $path,
            $filename = basename($url)
        );

        if (file_exists($filepath)) {
            return $filename;
        }

        $image = $this->request($url);

        file_put_contents($filepath, $image);

        logline('Saved image %s', $filename);

        return $filename;
    }

    public function getLatestLogUnixtime(): ?int
    {
        $stamp = $this
            ->db()
            ->query('SELECT MAX(logged) FROM log')
            ->fetchColumn(0);

        if (!$stamp) {
            return null;
        }

        return (int) DateTime::createFromFormat(
            'Y-m-d H:i:s',
            $stamp,
            new DateTimeZone('UTC')
        )->format('U');
    }
}

try {
    $script = basename(__FILE__);
    $opt = getopt('c:');

    if (!(isset($opt['c']) && file_exists($config = $opt['c']))) {
        echo "Usage: $script -c /path/to/config.ini\n";
        exit;
    }

    $scraper = new Scraper(parse_ini_file($config));

    $now = time();
    $last = $scraper->getLatestLogUnixtime();

    $first_page = $scraper->getRecentTracks(1, $now, $last);
    $pages = (int) $first_page['@attr']['totalPages'];

    while ($pages > 1) {
        sleep(1);

        $data = $scraper->getRecentTracks($pages, $now, $last);
        $scraper->saveTracks($data['track']);

        $pages--;
    }

    $scraper->saveTracks($first_page['track']);

    logline('Done!');
} catch (Exception $e) {
    logline('Exception! - %s', $e->getMessage());
}
